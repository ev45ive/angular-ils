import { AuthService } from './auth.service';
import { PlaylistsService } from './playlists/playlists.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistsListComponent } from './playlists/playlists-list.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { PlaylistItemComponent } from './playlists/playlist-item.component';

import { FormsModule } from '@angular/forms';
import { MusicModule } from './music/music.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { Routing } from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistDetailsComponent,
    PlaylistItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    Routing,
    MusicModule
  ],
  providers: [
    // {provide:'API_URL', useValue: 'malinowy'},
    // {provide:'API_OPTIONS', useFactory: function(API_URL){
    //   return 'Placki i API_URL ' + API_URL
    // }, deps:['API_URL']},
    // {provide:AbstractPlaylistsService, useClass: PlaylistsService}
    // {provide:PlaylistsService, useClass: PlaylistsService},
    PlaylistsService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private auth:AuthService){
    auth.getToken()
  }

}
