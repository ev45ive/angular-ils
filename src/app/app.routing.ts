import { MusicSearchComponent } from './music/music-search.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [
    // Home path:
    {
        path: '',
        redirectTo: 'playlists', pathMatch: 'full'
    },
    {
        path: 'playlists',
        component: PlaylistsComponent
    },
    {
        path: 'music',
        component: MusicSearchComponent
    },
    // Page not found:
    {
        path: '**',
        redirectTo: 'music'
    },
]

export const Routing = RouterModule.forRoot(routes, {
    // enableTracing: true,
    useHash: true
})