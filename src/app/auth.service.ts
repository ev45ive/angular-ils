import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
  // https://   api.spotify.com/v1/search?type=album&q=

  constructor() { }

  authorize(){
    let client_id="645cc0a298b94f1f9cd2709fc145e2b3"
    let redirect_uri="http://localhost:4200/"

    let url = `https://accounts.spotify.com/authorize?client_id=${client_id}&response_type=token&redirect_uri=${redirect_uri}`;
    
    location.hash = ''
    localStorage.removeItem('SPOTIFY_TOKEN')
    location.replace(url)
  }

  token:string

  getToken(){
    this.token = JSON.parse(localStorage.getItem('SPOTIFY_TOKEN'))

    if(!this.token){
      let match = location.hash.match(/#access_token=(.*?)&/)
      this.token = match && match[1]
    }

    if(!this.token){
      this.authorize()
    }
    localStorage.setItem('SPOTIFY_TOKEN', JSON.stringify(this.token))
    return this.token
  }
}
