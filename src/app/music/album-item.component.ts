import { selector } from 'rxjs/operator/publish';
import { Album } from './album';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'album-item',
  templateUrl: './album-item.component.html',
  styles: [`
    :host(){
      display:block;
    }
  `]
})
export class AlbumItemComponent implements OnInit {

  @Input()
  album:Album

  constructor() { }

  ngOnInit() {
  }

}
