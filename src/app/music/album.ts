interface Entity{
    id: string
    name: string
}

export interface Album  extends Entity{
    images: AlbumImage[]
    artists?: Artist[]
}

interface Artist extends Entity{}

interface AlbumImage{
    url: string
    width: number
    height: number
}