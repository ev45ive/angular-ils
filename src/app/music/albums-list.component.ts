import { Component, Input, OnInit } from '@angular/core';
import { Album } from './album';

@Component({
  selector: 'albums-list',
  templateUrl: './albums-list.component.html',
  styles: [`
  .card{
    max-width:25%;
    min-width:25%;
  }
`]
})
export class AlbumsListComponent implements OnInit {

  @Input()
  albums:Album[]

  constructor() { }

  ngOnInit() {
  }

}
