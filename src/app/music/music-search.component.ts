import { MusicService } from './music.service';
import { Component, OnInit } from '@angular/core';
import { Album } from './album';

@Component({
  selector: 'music-search',
  templateUrl: './music-search.component.html',
  styles: []
})
export class MusicSearchComponent implements OnInit {
  albums:Album[]

  constructor(private service:MusicService) { }

  search(query){
    this.service.getAlbums(query)
    .subscribe(albums => 
      this.albums = albums
    )
  }

  ngOnInit() {
    this.search('batman')
  }

}
