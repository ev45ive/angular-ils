import { AuthService } from '../auth.service';
import { Injectable } from '@angular/core';
import { Album } from './album';
import { HttpClient, HttpHeaders } from '@angular/common/http';


// http://reactivex.io/rxjs/
// http://rxmarbles.com/

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

@Injectable()
export class MusicService {
  albums:Album[] = []

  constructor(private http:HttpClient, private auth:AuthService) { }

  getAlbums(query = 'batman'){

    let url = `https://api.spotify.com/v1/search?type=album&q=${query}`

    return this.http.get(url,{
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.auth.getToken()
      })
    })
    .map( data => 
      <Album[]>data['albums'].items
    )
    .catch((err, caught) => {
      this.auth.authorize()
      return []
    })
  }

}
