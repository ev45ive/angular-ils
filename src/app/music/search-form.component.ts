import { FormControl, FormGroup, FormGroupDirective } from '@angular/forms';
import { Component, OnInit, Output } from '@angular/core';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styles: []
})
export class SearchFormComponent implements OnInit {

  queryForm:FormGroup

  constructor() {
    this.queryForm = new FormGroup({
      query: new FormControl('batman')
    })
    window['form'] = this.queryForm

    this.queryChange = this.queryForm.controls.query
    .valueChanges
    .debounceTime(400)
    .filter(query => query.length >= 3)
  }
  
  @Output()
  queryChange /* = new EventEmitter() */


  ngOnInit() {
  }

}
