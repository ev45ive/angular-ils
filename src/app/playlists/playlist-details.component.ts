import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from './playlist.interface';


enum MODES { show, edit }

@Component({
  selector: 'playlist-details',
  // template:``
  templateUrl: './playlist-details.component.html'
})
export class PlaylistDetailsComponent implements OnInit {

  constructor() {
  }
  
  ngOnInit() {
  }

  @Input('playlist')
  set setPlaylist(playlist){
    this.playlist = playlist
  }

  @Output('save')
  saveEmitter = new EventEmitter<Playlist>()
  
  playlist:Playlist
  draft:Playlist
  
  MODES = MODES

  save(){
    this.saveEmitter.emit(this.draft)
  }
  mode = MODES.show 

  cancel(){
    this.mode = MODES.show
  }

  edit(){
    this.draft = Object.assign({},this.playlist)
    this.mode = MODES.edit
  }



}
