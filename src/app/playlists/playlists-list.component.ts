import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from './playlist.interface';

@Component({
  selector: 'playlists-list',
  template: `
    <div class="list-group">
      <div class="list-group-item" 
        *ngFor="let playlist of playlists; let i = index"
        [class.active]="selected == playlist"

        (click)="select(playlist)"
        
        >{{i + 1}}. {{ playlist.name }}
      </div>
    </div>
    <div *ngIf="!playlists.length">
      No playlists!
    </div>
  `,
})
export class PlaylistsListComponent implements OnInit {
  @Output()
  selectedChange = new EventEmitter<Playlist>()

  @Input('playlists')
  playlists:Playlist[]
  
  @Input()
  selected:Playlist

  select(playlist:Playlist){
    this.selectedChange.emit(playlist)
  }

  ngOnInit() {
    // this.selected = this.playlists[2]
  }



  constructor() { }




}
