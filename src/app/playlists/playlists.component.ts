import { PlaylistsService } from './playlists.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Playlist } from './playlist.interface'

@Component({
  selector: 'playlists',
  template: `
  <div class="row">
    <div class="col">
    
      <playlists-list 
      [playlists]="playlists" 
      [selected]="selected"
      (selectedChange)="selected = $event"
    ></playlists-list>
     
    </div>
    <div class="col">
      <playlist-details 
      *ngIf="selected"
      (save)="save($event)"
      [playlist]="selected">
      </playlist-details>

      <div *ngIf="!selected"> 
      Please select Playlist </div>

    </div>
  </div>
  `
})
export class PlaylistsComponent implements OnInit {

  playlists:Playlist[] = []

  selected: Playlist

  ngOnInit() {
    //this.selected = this.playlists[1]
  }
  
  constructor(/* @Inject(PlaylistsService) */private service:PlaylistsService) {
    this.playlists = this.service.playlists
  }

  save(playlist){
    var found = this.playlists.findIndex( p => p.id == playlist.id)
    this.playlists.splice(found,1,playlist)

  }
  

}
