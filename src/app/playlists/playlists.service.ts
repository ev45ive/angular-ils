import { Injectable } from '@angular/core';
import { Playlist } from './playlist.interface';

@Injectable()
export class PlaylistsService {

  constructor() { }

  getPlaylists(){
    return this.playlists
  }

  playlists:Playlist[] = [
    {
      id: 1, 
      name: 'Angular Greatest Hits', 
      favourite: false,
      color: '#00FF00'
    },
    {
      id: 2, 
      name: 'Angular Top 20', 
      favourite: true,
      color: '#FF0000'
    },
    {
      id: 3, 
      name: 'Best Of A!', 
      favourite: false,
      color: '#0000FF'
    }
  ]


}
